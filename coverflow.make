api = 2
core = 7.x

libraries[coverflow][download][type] = git
libraries[coverflow][download][tag] = 3.0.1
libraries[coverflow][download][url] = https://github.com/coverflowjs/coverflow.git
libraries[coverflow][destination] = libraries

libraries[jquery][download][type] = file
libraries[jquery][download][url] = https://raw.githubusercontent.com/jquery/jquery/2.1.3/dist/jquery.min.js
libraries[jquery][download][filename] = jquery-2.1.3.min.js