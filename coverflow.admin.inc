<?php

/**
 * @file
 * Module administration form.
 */

function coverflow_admin_form($form, $form_state) {
  drupal_set_title('Coverflow module configuration');

  $form['coverflow_options'] = array(
    '#type' => 'textarea',
    '#title' => t('Options'),
    '#description' => t('JSON encoded.'),
    '#default_value' => variable_get('coverflow_options', '')
  );

  return system_settings_form($form);
}
