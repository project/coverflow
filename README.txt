CONFIGURATION
-------------
 * In jQuery Multi Settings, make coverflow use jQuery 2.1.3

 * The coverflow module adds a checkbox to enable it on any multiple field display settings.
