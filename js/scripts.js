/**
 * @file
 * Library initialization.
 */

(function ($) {
    Drupal.behaviors.coverflow = {
        attach: function (context, settings) {
            $('.field-coverflow>.field-items').coverflow(JSON.parse(Drupal.settings.coverflow.options));
            $('.field-coverflow').each(function(index, element) {
                var $element = $(element);
                $element.css('min-height', $element.find('>.field-items').height());
            });
        }
    };
})(jq213);
